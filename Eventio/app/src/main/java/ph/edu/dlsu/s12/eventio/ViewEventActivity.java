package ph.edu.dlsu.s12.eventio;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.Calendar;

public class ViewEventActivity extends AppCompatActivity {

    private EditText event_date, time, venue;
    private TextView header, registered, attended, event_score;
    private TextView text_date, text_time, text_venue;
    private Button btn_pending, btn_ongoing, btn_completed, btn_edit, btn_done,btn_survey;
    private ImageView qr_code;
    private String status;
    public String user_type;
    public String event_name;
    public String email;
    public boolean disable_buttons = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Initialize elements and essential variables i.e user information, event information
        if(getIntent().getExtras() != null) {
            event_name = getIntent().getStringExtra("event_name");
            user_type = getIntent().getStringExtra("user_type");
            email = getIntent().getStringExtra("email");

            dbHelper dbHelper = new dbHelper(getApplicationContext());
            Event event_details = dbHelper.readEvent(event_name);

            status = event_details.getStatus();

            ArrayList<RegisteredUsers> registered_users = dbHelper.getListPerType(event_name, "R");
            ArrayList<RegisteredUsers> attended_users = dbHelper.getListPerType(event_name, "A");

            setContentView(R.layout.activity_view_event_organizer);

            toggleView();
            initEvent(dbHelper, event_details, registered_users, attended_users);

            if (user_type.equals("Organizer"))
                statusHandler(dbHelper.readEvent(event_name).getStatus());
        }

        // Calls a date picker dialog box for inputting time
        event_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                DatePickerDialog datePickerDialog = new DatePickerDialog(ViewEventActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
                                event_date.setText(dayOfMonth + "/"
                                        + (monthOfYear + 1) + "/" + year);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        // Calls a time picker dialog box for inputting time
        time.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(ViewEventActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        String minute = "";

                        if (selectedMinute < 10)
                            minute = "0"+selectedMinute;
                        else
                            minute = String.valueOf(selectedMinute);

                        time.setText( selectedHour + ":" + minute);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });
    }

    // Function to initialize page elements and assign event information
    // Some elements are hidden or shown depending on the user type
    private void initEvent(dbHelper dbHelper, Event event_details, ArrayList<RegisteredUsers> registered_users, ArrayList<RegisteredUsers> attended_users) {
        if (user_type.equals("Organizer")) {
            this.btn_edit = findViewById(R.id.btn_edit);

            readEventInfo(event_details, registered_users, attended_users);

            btn_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    header = findViewById(R.id.header);
                    event_date = findViewById(R.id.event_date);
                    time = findViewById(R.id.time);
                    venue = findViewById(R.id.venue);
                    registered = findViewById(R.id.registered);
                    attended = findViewById(R.id.attended);
                    event_score = findViewById(R.id.event_score);
                    qr_code = findViewById(R.id.qr_code);

                    if (validateFields(header.getText().toString(), event_date.getText().toString(),
                            time.getText().toString(), venue.getText().toString())) {

                        Event new_event = new Event(header.getText().toString(), status, event_date.getText().toString(),
                                time.getText().toString(), venue.getText().toString(), event_details.getScore(), event_details.getSurvey(), event_details.getQR(), event_details.getCreator());

                        dbHelper.updateEvent(new_event);

                        // If event is updated to be complete, system sends out an email to all the registered users in the event
                        if (new_event.getStatus().equals("Completed") && new_event.getSurvey() == 1 ) {
                            ArrayList<RegisteredUsers> event_list = dbHelper.getAllRegistered(new_event.getEventName());
                            User temp_user;
                            String name = "";
                            String recipient = "";
                            String body = "";
                            String subject = "Eventio - Survey Form: "+new_event.getEventName();

                            for (int i = 0; i < event_list.size(); i++) {
                                temp_user = event_list.get(i).getUser();
                                recipient = temp_user.getEmailAddress();
                                name = temp_user.getLastName()+", "+temp_user.getFirstName();

                                body = "Good day "+name+",\n Thank you for attending one of our partnered events. We would like to inform you of a pending survey form associated with the event you just attended. Thank you.";

                                sendEmail(recipient, subject, body);
                            }
                        }

                        Intent intent = new Intent(ViewEventActivity.this, ViewEventActivity.class);
                        intent.putExtra("event_name", event_name);
                        intent.putExtra("user_type", user_type);
                        intent.putExtra("email", email);
                        startActivity(intent);
                        finish();
                    }
                }
            });
        }
        else {
            this.btn_done = findViewById(R.id.btn_done);
            this.btn_survey = findViewById(R.id.btn_survey);
            btn_survey.setVisibility(View.VISIBLE);
            readEventInfo(event_details, registered_users, attended_users);

            btn_done.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(ViewEventActivity.this, HomeActivity.class);
                    intent.putExtra("event_name", event_name);
                    intent.putExtra("user_type", user_type);
                    intent.putExtra("email", email);
                    startActivity(intent);
                    finish();
                }
            });

            btn_survey.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(ViewEventActivity.this, SurveyActivity.class);
                    intent.putExtra("event_name", event_name);
                    intent.putExtra("user_type", user_type);
                    intent.putExtra("email", email);
                    startActivity(intent);
                    finish();
                }
            });
        }
    }

    // Function to use the GMailSender class which uses javax.mail.authenticator API to send an email
    private void sendEmail(String recipient, String subject, String body) {
        String pass = "^gs6:T#f-\\jCt';*";
        try {
            GMailSender sender = new GMailSender("email.api.trial@gmail.com", pass);
            sender.sendMail(subject,
                    body,
                    "email.api.trial@gmail.com",
                    recipient);
        } catch (Exception e) {
            Log.e("SendMail", e.getMessage(), e);
        }
    }

    // This function disables editing of certain input fields depending on the current
    // event status
    private void statusHandler(String status) {
        if (status.equals("Completed")){
            text_date = findViewById(R.id.text_date);
            text_time = findViewById(R.id.text_time);
            text_venue = findViewById(R.id.text_venue);

            text_date.setVisibility(View.VISIBLE);
            text_time.setVisibility(View.VISIBLE);
            text_venue.setVisibility(View.VISIBLE);

            event_date = findViewById(R.id.event_date);
            time = findViewById(R.id.time);
            venue = findViewById(R.id.venue);
            btn_edit = findViewById(R.id.btn_edit);

            event_date.setVisibility(View.GONE);
            time.setVisibility(View.GONE);
            venue.setVisibility(View.GONE);
            btn_edit.setVisibility(View.GONE);

            disable_buttons = true;
        }
        else if (status.equals("On-going")) {
            text_date = findViewById(R.id.text_date);
            text_time = findViewById(R.id.text_time);
            text_venue = findViewById(R.id.text_venue);

            text_date.setVisibility(View.VISIBLE);
            text_time.setVisibility(View.VISIBLE);
            text_venue.setVisibility(View.VISIBLE);

            event_date = findViewById(R.id.event_date);
            time = findViewById(R.id.time);
            venue = findViewById(R.id.venue);

            event_date.setVisibility(View.GONE);
            time.setVisibility(View.GONE);
            venue.setVisibility(View.GONE);
        }
    }

    // This function assigns the associated event data with the elements in the page
    private void readEventInfo(Event event_details, ArrayList<RegisteredUsers> registered_users, ArrayList<RegisteredUsers> attended_users) {
        if (event_details != null) {
            String score;
            int registered_len = 0;
            int attended_len = 0;

            header = findViewById(R.id.header);

            event_date = findViewById(R.id.event_date);
            time = findViewById(R.id.time);
            venue = findViewById(R.id.venue);

            text_date = findViewById(R.id.text_date);
            text_time = findViewById(R.id.text_time);
            text_venue = findViewById(R.id.text_venue);

            registered = findViewById(R.id.registered);
            attended = findViewById(R.id.attended);
            event_score = findViewById(R.id.event_score);
            qr_code = findViewById(R.id.qr_code);

            if (event_details.getScore().toString().equals("0.0")) {
                score = "N/A";
            }
            else {
                score = event_details.getScore().toString();
            }

            header.setText(event_details.getEventName());

            event_date.setText(event_details.getDate());
            time.setText(event_details.getTime());
            venue.setText(event_details.getVenue());

            text_date.setText(event_details.getDate());
            text_time.setText(event_details.getTime());
            text_venue.setText(event_details.getVenue());

            if (registered_users != null) {
                registered_len = registered_users.size()+attended_users.size();
            }
            if (attended_users != null) {
                attended_len = attended_users.size();
            }

            registered.setText("Registered: "+registered_len);
            attended.setText("Attended: "+attended_len);
            event_score.setText("Event Rating: "+score);
            qr_code.setImageBitmap(converttoBitmap(event_details.getQR()));

            toggleStatusButton(event_details.getStatus());
        }
    }

    // This function converts byte array to a bitmap data type which is used to set the generated QR code
    // in the ImageView
    private Bitmap converttoBitmap(byte[] byteArray) {
        Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);

        return bmp;
    }

    // Function to check if all the user inputs are valid i.e no required fields which are empty
    public Boolean validateFields(String event_name, String date, String time, String venue) {
        Boolean res = true;
        if (event_name.trim().length() == 0) {
            res = false;
        }
        if (date.trim().length() == 0) {
            res = false;
        }
        if (time.trim().length() == 0) {
            res = false;
        }
        if (venue.trim().length() == 0) {
            res = false;
        }

        if(!res) {
            Toast.makeText(getApplicationContext(), "Missing field!", Toast.LENGTH_SHORT).show();
        }
        return res;
    }

    // Function to initialize and hide certain elements depending on the account type of the logged in user
    public void toggleView() {
        if (user_type.equals("Organizer")) {
            text_date = findViewById(R.id.text_date);
            text_time = findViewById(R.id.text_time);
            text_venue = findViewById(R.id.text_venue);
            btn_done = findViewById(R.id.btn_done);

            text_date.setVisibility(View.GONE);
            text_time.setVisibility(View.GONE);
            text_venue.setVisibility(View.GONE);
            btn_done.setVisibility(View.GONE);
        }
        else {
            event_date = findViewById(R.id.event_date);
            time = findViewById(R.id.time);
            venue = findViewById(R.id.venue);
            btn_edit = findViewById(R.id.btn_edit);

            event_date.setVisibility(View.GONE);
            time.setVisibility(View.GONE);
            venue.setVisibility(View.GONE);
            btn_edit.setVisibility(View.GONE);
        }
    }

    // This function is used to perform logical assignments for event status depending on the last button clicked
    public void toggleStatusButton(String e_status) {
        btn_pending = findViewById(R.id.btn_pending);
        btn_ongoing = findViewById(R.id.btn_ongoing);
        btn_completed = findViewById(R.id.btn_completed);

        if (e_status.equals("Pending")) {
            btn_pending.getBackground().setAlpha(255);
            btn_ongoing.getBackground().setAlpha(40);
            btn_completed.getBackground().setAlpha(40);
            status = "Pending";
        }
        else if (e_status.equals("On-going")) {
            btn_ongoing.getBackground().setAlpha(255);
            btn_pending.getBackground().setAlpha(40);
            btn_completed.getBackground().setAlpha(40);
            status = "On-going";
        }
        else if (e_status.equals("Completed")) {
            btn_completed.getBackground().setAlpha(255);
            btn_ongoing.getBackground().setAlpha(40);
            btn_pending.getBackground().setAlpha(40);
            status = "Completed";
        }
    }

    // This function is called when a status button is clicked which calls on a function to perform logical operations and conditions
    public void onStatusButtonClicked(View view) {
        if (user_type.equals("Organizer") && !disable_buttons) {
            switch (view.getId()) {
                case R.id.btn_pending:
                    break;
                case R.id.btn_ongoing:
                    status = "On-going";
                    toggleStatusButton(status);
                    break;
                case R.id.btn_completed:
                    status = "Completed";
                    toggleStatusButton(status);
                    break;

            }
        }
    }
}
