package ph.edu.dlsu.s12.eventio;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.IOException;

public class ScanQRActivity extends AppCompatActivity {

    SurfaceView surfaceView;
    TextView txtBarcodeValue;
    private BarcodeDetector barcodeDetector;
    private CameraSource cameraSource;
    private static final int REQUEST_CAMERA_PERMISSION = 201;
    Button btnAction;
    String intentData = "";
    public String email;
    public String user_type;
    boolean isEmail = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_qr);

        user_type = getIntent().getStringExtra("user_type");
        email = getIntent().getStringExtra("email");

        initViews();
    }

    private void initViews() {
        txtBarcodeValue = findViewById(R.id.txtBarcodeValue);
        surfaceView = findViewById(R.id.surfaceView);
        btnAction = findViewById(R.id.btnAction);


        btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (intentData.length() > 0) {
                    if(validateEventExists(intentData)) {
                        Log.d("intent", intentData);
                        dbHelper dbHelper = new dbHelper(getApplicationContext());

                        registerToEvent(dbHelper, intentData, email);

                        Intent intent = new Intent(ScanQRActivity.this, HomeActivity.class);
                        intent.putExtra("user_type", user_type);
                        intent.putExtra("email", email);
                        startActivity(intent);
                        finish();
                    }
                    else {
                        Toast.makeText(ScanQRActivity.this, "QR does not exist in database!", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });
    }

    private void initialiseDetectorsAndSources() {

        Toast.makeText(getApplicationContext(), "Barcode scanner started", Toast.LENGTH_SHORT).show();

        barcodeDetector = new BarcodeDetector.Builder(getApplicationContext())
                .setBarcodeFormats(Barcode.ALL_FORMATS)
                .build();

        cameraSource = new CameraSource.Builder(getApplicationContext(), barcodeDetector)
                .setRequestedPreviewSize(1920, 1080)
                .setAutoFocusEnabled(true)
                .build();

        surfaceView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                try {
                    if (ActivityCompat.checkSelfPermission(ScanQRActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        cameraSource.start(surfaceView.getHolder());
                    } else {
                        ActivityCompat.requestPermissions(ScanQRActivity.this, new
                                String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                cameraSource.stop();
            }
        });


        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {
                Toast.makeText(getApplicationContext(), "Scanner has been stopped", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> barcodes = detections.getDetectedItems();
                if (barcodes.size() != 0) {

                    txtBarcodeValue.post(new Runnable() {

                        @Override
                        public void run() {
                            btnAction.setText("Register to Event");
                            intentData = barcodes.valueAt(0).displayValue;
                            txtBarcodeValue.setText("Event key: "+intentData);
                        }
                    });

                }

            }
        });
    }

    private boolean validateEventExists(String key) {
        Boolean exists = false;
        dbHelper dbHelper = new dbHelper(getApplicationContext());
        Event event = dbHelper.readEvent(key);

        if (event != null) {
            exists = true;
        }
        return exists;
    }

    private void registerToEvent(dbHelper dbHelper, String event, String email) {
        Log.d("input", event+" - "+email);
        if (dbHelper.readRegistered(event, email) == null && !(dbHelper.readEvent(event).getStatus().equals("Completed"))) {
            dbHelper.eventRegistration(event, email);
        }
        else {
            Event event_details = dbHelper.readEvent(event);

            if (event_details.getStatus().equals("Pending")) {
                dbHelper.updateRegistration(event, email, "R");
            }
            else if (event_details.getStatus().equals("On-going")) {
                dbHelper.updateRegistration(event, email, "A");
            }
            else {
                Toast.makeText(this, "Event has already finished cannot register", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        cameraSource.release();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initialiseDetectorsAndSources();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(ScanQRActivity.this, HomeActivity.class);
        intent.putExtra("user_type", user_type);
        intent.putExtra("email", email);
        startActivity(intent);
        finish();
    }
}

