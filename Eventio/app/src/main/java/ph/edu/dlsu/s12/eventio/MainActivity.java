package ph.edu.dlsu.s12.eventio;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private User user;
    private EditText inp_username, inp_password;
    private Button btn_login, btn_register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.btn_login = findViewById(R.id.btn_login);
        this.btn_register = findViewById(R.id.btn_register);

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, HomeActivity.class);

                if (checkCredentials(intent) == 1) {
                    startActivity(intent);
                    finish();
                }
            }
        });

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, RegisterActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    // Function to validate the user credentials with the database and initializes the intent extras if valid
    private int checkCredentials(Intent intent) {
        dbHelper dbHelper = new dbHelper(getApplicationContext());

        this.inp_username = findViewById(R.id.inp_username);
        this.inp_password = findViewById(R.id.inp_password);

        String username = inp_username.getText().toString();
        String password = inp_password.getText().toString();

        user = dbHelper.readUser(username);
        if (user != null) {
            if (user.getPassword().equals(password)) {
                intent.putExtra("user_type", user.getUserType()).putExtra("email", user.getEmailAddress());
                return 1;
            }
            else {
                Toast.makeText(getApplicationContext(), "Invalid password", Toast.LENGTH_SHORT).show();
                return 0;
            }
        }
        else {
            Toast.makeText(getApplicationContext(), "User does not exist", Toast.LENGTH_SHORT).show();
            return 0;
        }
    }
}