package ph.edu.dlsu.s12.eventio;

public class User {
    private String last_name, first_name, email_address, mobile_num, password, user_type, birthday, organization;

    public User(String first_name, String last_name, String email_address, String mobile_num, String password, String user_type,
                String birthday, String organization) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.email_address = email_address;
        this.mobile_num = mobile_num;
        this.password = password;
        this.user_type = user_type;
        this.birthday = birthday;
        this.organization = organization;
    };

    public String getLastName() {
        return last_name;
    }
    public void setLastName(String last_name) {
        this.last_name = last_name;
    }

    public String getFirstName() {
        return first_name;
    }
    public void setFirstName(String first_name) {
        this.first_name = first_name;
    }

    public String getEmailAddress() {
        return email_address;
    }
    public void setEmailAddress(String email_address) {
        this.email_address = email_address;
    }

    public String getMobileNumber() {
        return mobile_num;
    }
    public void setMobileNumber(String mobile_num) {
        this.mobile_num = mobile_num;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserType() {
        return user_type;
    }
    public void setUserType(String user_type) {
        this.user_type = user_type;
    }

    public String getBirthday() {
        return birthday;
    }
    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getOrganization() {
        return organization;
    }
    public void setOrganization(String organization) {
        this.organization = organization;
    }
}
