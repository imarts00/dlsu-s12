package ph.edu.dlsu.s12.eventio;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class EventAdapter extends ArrayAdapter<Event> {
    private Activity activity;
    private ArrayList<Event> eventArrayList;

    public EventAdapter(Activity activity, ArrayList<Event> eventArrayList) {
        super(activity, R.layout.event_card, eventArrayList);
        this.activity = activity;
        this.eventArrayList = eventArrayList;
    };

    public View getView(int pos, View convertView, ViewGroup parent) {
        View rowView = convertView;

        if (rowView == null) {
            LayoutInflater inflater = activity.getLayoutInflater();
            rowView = inflater.inflate(R.layout.event_card, null);

            ViewHolder cardViewHolder = new ViewHolder();

            cardViewHolder.event_name = (TextView) rowView.findViewById((R.id.event_name));
            cardViewHolder.event_score = (TextView) rowView.findViewById((R.id.event_score));
            cardViewHolder.date = (TextView) rowView.findViewById((R.id.date));
            cardViewHolder.venue = (TextView) rowView.findViewById((R.id.venue));
            cardViewHolder.status = (TextView) rowView.findViewById((R.id.status));

            rowView.setTag(cardViewHolder);
        }

        final ViewHolder holder = (ViewHolder) rowView.getTag();
        Event event = getItem(pos);
        holder.event_name.setText(event.getEventName());
        holder.date.setText(event.getDate()+"   "+event.getTime());
        holder.venue.setText(event.getVenue());
        holder.status.setText(event.getStatus());

        if (!event.getScore().toString().equals("0.0")) {
            holder.event_score.setText(event.getScore().toString());
        }
        else {
            holder.event_score.setText("");
        }
        return rowView;
    };

    static class ViewHolder{
        public TextView event_name;
        public TextView event_score;
        public TextView date;
        public TextView venue;
        public TextView status;
    }
}
