package ph.edu.dlsu.s12.eventio;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity {

    private EventAdapter eventAdapter;
    private EventStatusAdapter statusAdapter;
    public ArrayList<EventStatus> statusList = new ArrayList<EventStatus>();

    private ArrayList<Event> eventArrayList;
    private Button btn_submit_filter, btn_add_event, btn_logout;
    private Spinner spinner;
    private ImageButton btn_camera;
    private ListView event_list;
    public String email;

    // Intitialize status elements for spinner dropdown
    protected void initializeStatusList() {
        EventStatus obj = new EventStatus("Filter by: ", false);
        statusList.add(obj);
        obj = new EventStatus("Pending", true);
        statusList.add(obj);
        obj = new EventStatus("On-going", true);
        statusList.add(obj);
        obj = new EventStatus("Completed", true);
        statusList.add(obj);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initializeStatusList();

        if(getIntent().getExtras() != null) {
            setContentView(R.layout.activity_home_organizer);

            String user_type = getIntent().getStringExtra("user_type");
            email = getIntent().getStringExtra("email");
            dbHelper dbHelper = new dbHelper(getApplicationContext());

            toggleView(dbHelper, user_type);

            populateSpinner(statusList);

            this.btn_logout = findViewById(R.id.btn_logout);

            // Logout user
            btn_logout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(HomeActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
        }
    }

    // Function to initialize and hide certain elements depending on the account type of the logged in user
    public void toggleView(dbHelper dbHelper, String user_type) {
        this.event_list = findViewById(R.id.event_list);
        this.btn_submit_filter = findViewById(R.id.btn_submit_filter);

        if (user_type.equals("Organizer")) {
            btn_add_event = findViewById(R.id.btn_add_event);
            btn_camera = findViewById(R.id.btn_camera);

            btn_camera.setVisibility(View.GONE);

            populateEvents(dbHelper.getAllEvents(null, email), user_type);

            btn_add_event.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(HomeActivity.this, CreateEventActivity.class);
                    intent.putExtra("user_type", user_type).putExtra("email", email);
                    startActivity(intent);

                }
            });
        }
        else {
            btn_add_event = findViewById(R.id.btn_add_event);
            btn_camera = findViewById(R.id.btn_camera);
            btn_add_event.setVisibility(View.GONE);

            populateEvents(dbHelper.getAllAttendeeEvents(null, email), user_type);

            btn_camera.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(HomeActivity.this, ScanQRActivity.class).
                    putExtra("user_type", user_type).putExtra("email", email);
                    startActivity(intent);
                    finish();
                }
            });
        }

        btn_submit_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> filter_arr = new ArrayList<String>();

                for (int i = 1; i < statusList.size(); i++) {
                    if (statusList.get(i).getState()) {
                        filter_arr.add(statusList.get(i).getStatusName());
                    }
                }

                if (user_type.equals("Organizer"))
                    populateEvents(dbHelper.getAllEvents(filter_arr, email), user_type);
                else
                    populateEvents(dbHelper.getAllAttendeeEvents(filter_arr, email), user_type);
            }
        });
    }

    // Function to set the adapter and onclick listener of each event card element
    public void populateEvents(ArrayList<Event> eventArrayList, String user_type) {
        this.event_list = findViewById(R.id.event_list);

        eventAdapter = new EventAdapter(this, eventArrayList);
        event_list.setAdapter(eventAdapter);
        event_list.deferNotifyDataSetChanged();

        event_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Event event = eventArrayList.get(position);
                String view_event = event.getEventName();

                Intent intent = new Intent(HomeActivity.this, ViewEventActivity.class);
                intent.putExtra("event_name", view_event);
                intent.putExtra("user_type", user_type);
                intent.putExtra("email", email);
                startActivity(intent);

            }
        });
    }

    // Function to set the adapter and listener of the spinner element
    public void populateSpinner(ArrayList<EventStatus> statusArrayList) {
        this.spinner = findViewById(R.id.spinner);

        statusAdapter = new EventStatusAdapter(this, statusArrayList);
        spinner.setAdapter(statusAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(HomeActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
