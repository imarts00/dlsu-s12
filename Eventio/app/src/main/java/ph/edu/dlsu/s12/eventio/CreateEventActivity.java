package ph.edu.dlsu.s12.eventio;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.zxing.WriterException;

import java.io.ByteArrayOutputStream;
import java.util.Calendar;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;

public class CreateEventActivity extends AppCompatActivity {

    private Event event;
    private EditText event_name, date, time, venue;
    public int survey = 0;
    private CheckBox checkbox_survey;
    private Button btn_create;
    private DatePickerDialog datePickerDialog;
    private TimePickerDialog timePickerDialog;
    private QRGEncoder qrgencoder;
    public String email;
    public String user_type;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_event);

        // Initialize elements and retrieve sent extra
        this.event_name = findViewById(R.id.event_name);
        this.date = findViewById(R.id.date);
        this.time = findViewById(R.id.time);
        this.venue = findViewById(R.id.venue);
        this.checkbox_survey = findViewById(R.id.checkbox_survey);
        this.btn_create = findViewById(R.id.btn_create);

        user_type = getIntent().getStringExtra("user_type");
        email = getIntent().getStringExtra("email");

        // Calls a date picker dialog box for inputting time
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                datePickerDialog = new DatePickerDialog(CreateEventActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
                                date.setText(dayOfMonth + "/"
                                        + (monthOfYear + 1) + "/" + year);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        // Calls a time picker dialog box for inputting time
        time.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(CreateEventActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        String minute = "";

                        if (selectedMinute < 10)
                            minute = "0"+selectedMinute;
                        else
                            minute = String.valueOf(selectedMinute);

                        time.setText( selectedHour + ":" + minute);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });

        // Validate input fields before generating QR code and event object used for storing to database
        btn_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateFields(event_name.getText().toString(), date.getText().toString(),
                        time.getText().toString(), venue.getText().toString(), survey)) {

                    Bitmap bitmap = generateQR(event_name.getText().toString());

                    event = new Event(event_name.getText().toString(),"Pending", date.getText().toString(),
                            time.getText().toString(), venue.getText().toString(), null, survey, convertToByte(bitmap), email);

                    postEvent(event);
                    Intent intent = new Intent(CreateEventActivity.this, HomeActivity.class);
                    intent.putExtra("user_type", user_type);
                    intent.putExtra("email", email);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    // Function to convert from bitmap to byte array as database only supports blob data type for storing images
    private byte[] convertToByte (Bitmap bmp) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();

        return byteArray;
    }

    // Function to generate a bitmap QR code using the event name
    private Bitmap generateQR(String event_name) {
        Bitmap bitmap = null;

        WindowManager manager = (WindowManager) getSystemService(WINDOW_SERVICE);

        Display display = manager.getDefaultDisplay();

        Point point = new Point();
        display.getSize(point);

        int width = point.x;
        int height = point.y;

        int dimension = width < height ? width : height;
        dimension = dimension * 3 / 4;

        qrgencoder = new QRGEncoder(event_name, null, QRGContents.Type.TEXT, dimension);

        try {
            bitmap = qrgencoder.encodeAsBitmap();
        }
        catch (WriterException e) {
            Log.d("error", e.toString());
        }
        return bitmap;
    }

    public boolean eventExists(String event_name) {
        dbHelper dbHelper = new dbHelper(getApplicationContext());
        boolean res = false;
        Event event = dbHelper.readEvent(event_name);

        if (event != null) {
            res = true;
        }

        return res;
    }

    // Calls the databasehelper class to store the event details
    private void postEvent(Event event) {
        dbHelper dbHelper = new dbHelper(getApplicationContext());

        dbHelper.createEvent(event);
    }

    // Function to check if all the user inputs are valid i.e no required fields which are empty
    private Boolean validateFields(String event_name, String date, String time, String venue, int survey) {
        Boolean res = true;
        if (event_name.trim().length() == 0) {
            res = false;
        }
        if (date.trim().length() == 0) {
            res = false;
        }
        if (time.trim().length() == 0) {
            res = false;
        }
        if (venue.trim().length() == 0) {
            res = false;
        }

        if(!res) {
            Toast.makeText(getApplicationContext(), "Missing field!", Toast.LENGTH_SHORT).show();
        }

        if (eventExists(event_name.trim())) {
            Toast.makeText(getApplicationContext(), "Event name already exists!", Toast.LENGTH_LONG).show();
            res = false;
        }
        return res;
    }

    // Android function called on button press of checkbox
    public void onCheckboxClicked(View view) {
        this.checkbox_survey = findViewById(R.id.checkbox_survey);

        boolean stat = checkbox_survey.isChecked();

        if (stat)
            survey = 1;
        else
            survey = 0;
    }
}
