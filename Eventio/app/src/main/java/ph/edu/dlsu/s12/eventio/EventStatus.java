package ph.edu.dlsu.s12.eventio;

public class EventStatus {
    private String status_name;
    private boolean toggled;

    public EventStatus(String status_name, boolean toggled) {
        this.status_name = status_name;
        this.toggled = toggled;
    }

    public String getStatusName() { return status_name; }
    public void setStatusName(String status_name) {
        this.status_name = status_name;
    }

    public boolean getState() { return toggled; }
    public void toggleState(boolean state) {
        this.toggled = state;
    }
}
