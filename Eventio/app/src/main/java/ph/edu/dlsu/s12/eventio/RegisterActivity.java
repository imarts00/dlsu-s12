package ph.edu.dlsu.s12.eventio;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Calendar;

public class RegisterActivity extends AppCompatActivity {

    private User user;
    private EditText last_name, first_name, email_address,
            mobile_number, password, confirm_password,
            birthday, organization;
    private DatePickerDialog datePickerDialog;
    private Button btn_register;
    private RadioButton btn_attendee, btn_organizer;
    public String email;
    public String user_type;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        // Initialize elements
        this.btn_register = findViewById(R.id.btn_register);
        this.last_name = findViewById(R.id.last_name);
        this.first_name = findViewById(R.id.first_name);
        this.email_address = findViewById(R.id.email_address);
        this.mobile_number = findViewById(R.id.mobile_number);
        this.password = findViewById(R.id.password);
        this.confirm_password = findViewById(R.id.confirm_password);
        this.btn_attendee = findViewById(R.id.btn_attendee);
        this.btn_organizer = findViewById(R.id.btn_organizer);
        this.birthday = findViewById(R.id.birthday);
        this.organization = findViewById(R.id.organization);

        if (btn_attendee.isChecked()) {
            user_type = "Attendee";
        }
        else {
            user_type = "Organizer";
        }

        // Calls a date picker dialog box for inputting time
        birthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                datePickerDialog = new DatePickerDialog(RegisterActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
                                birthday.setText(dayOfMonth + "/"
                                        + (monthOfYear + 1) + "/" + year);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        // Validates the user input fields and creates a User object to be passed
        // onto the postRegistration function for storing in the database
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateFields(last_name.getText().toString(), first_name.getText().toString(), email_address.getText().toString(),
                        mobile_number.getText().toString(), password.getText().toString(), confirm_password.getText().toString(), birthday.getText().toString(),
                        organization.getText().toString())) {

                    user = new User(first_name.getText().toString(), last_name.getText().toString(), email_address.getText().toString(),
                            mobile_number.getText().toString(), password.getText().toString(), user_type, birthday.getText().toString(),
                            organization.getText().toString());

                    postRegistration(user);

                    Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    // This function verifies if the associated user inputs already exists in the database
    // and creates a user account if it passess the verification
    private void postRegistration(User user) {
        dbHelper dbHelper = new dbHelper(getApplicationContext());

        if(dbHelper.readUser(user.getEmailAddress()) == null) {
            dbHelper.createUser(user);

            Toast.makeText(getApplicationContext(), "Sucessfully added user!", Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(getApplicationContext(), "User already exists!", Toast.LENGTH_SHORT).show();
        }
    }

    // Function to validate if all the specified require fields are not empty
    public boolean validateFields(String last_name, String first_name, String email_address, String mobile_number,
                                  String password, String confirm_password, String birthday, String organization) {
        String msg = "";
        int count = 0;

        Boolean res = true;
        if (last_name.trim().length() == 0) {
            res = false;
        }
        if (first_name.trim().length() == 0) {
            res = false;
        }
        if (email_address.trim().length() == 0) {
            res = false;
        }
        if (!isValidEmail(email_address.trim())) {
            msg += "Please input a valid email address";
            count++;
        }
        if (mobile_number.trim().length() == 0) {
            res = false;
        }
        if (!mobile_number.trim().matches("^[0-9-]+$")) {
            if (count >= 1)
                msg += "\n";
            msg += "Please input a valid mobile number (numbers only)";
            count++;
            res = false;
        }
        if (password.trim().length() <= 7) {
            res = false;
            if (count >= 1)
                msg += "\n";
            msg += "Password should be more than 8 characters";
            count++;
        }
        if (confirm_password.trim().length() == 0) {
            res = false;
        }
        if (birthday.trim().length() == 0) {
            res = false;
        }
        if (organization.trim().length() == 0) {
            res = false;
        }

        if(!res) {
            if (count >= 1)
                msg += "\n";
            msg +="Missing field!";
        }

        if(!password.equals(confirm_password)) {
            Toast.makeText(getApplicationContext(), "Passwords do not match please try again", Toast.LENGTH_SHORT).show();
            res = false;
        }

        if (!msg.equals("")) {
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
        }

        return res;
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    // Called function to assign attributes on button click
    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();

        switch (view.getId()) {
            case R.id.btn_attendee:
                if (checked) {
                    user_type = "Attendee";
                }
                break;
            case R.id.btn_organizer:
                if (checked) {
                    user_type = "Organizer";
                }
                break;
        }
    }
}
