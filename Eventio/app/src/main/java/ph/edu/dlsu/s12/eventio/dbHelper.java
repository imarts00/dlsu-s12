package ph.edu.dlsu.s12.eventio;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class dbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "mydb.db";
    private static final String TABLE_USER = "user";

    private static final String COLUMN_LAST_NAME = "last_name";
    private static final String COLUMN_FIRST_NAME = "first_name";
    private static final String COLUMN_EMAIL = "email_address";
    private static final String COLUMN_MOBILE_NUM = "mobile_number";
    private static final String COLUMN_PASSWORD = "password";
    private static final String COLUMN_TYPE = "user_type";

    public dbHelper (Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table if not exists users " + "" +
                "(user_id integer primary key autoincrement, last_name text, first_name text, email_address text unique," +
                "mobile_num text, password text, user_type text, birthday text, organization text)"
        );

        db.execSQL("create table if not exists events " + "" +
                "(event_id integer primary key, event_name text, status text, date text, time text," +
                "venue text, score double, survey integer, qr blob, created_by text)"
        );

        db.execSQL("create table if not exists event_registrations " + "" +
                "(id integer primary key, event_name text, user_email text, type text, survey_received integer)"
        );

        db.execSQL("create table if not exists survey_scores " + "" +
                "(id integer primary key, event_name text, user_email text, q1 integer, q2 integer, q3 integer, q4 integer, q5 integer, q6 text, q7 text)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS users");
        db.execSQL("DROP TABLE IF EXISTS events");
        db.execSQL("DROP TABLE IF EXISTS event_registrations");
        db.execSQL("DROP TABLE IF EXISTS survey_scores");
        onCreate(db);
    }

    public void updateEventScore(String event_key) {
        double avg_score = 0;
        int n = 0;
        String[] columns = {"q1", "q2", "q3", "q4", "q5"};

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =  db.rawQuery( "select * from survey_scores where event_name= ?", new String[]{event_key});

        int cursorCount = cursor.getCount();
        if (cursor.moveToFirst()) {
            do {
                for (int i = 0; i < columns.length; i++) {
                    avg_score += cursor.getInt(cursor.getColumnIndex(columns[i]));
                    n++;
                }
            }
            while (cursor.moveToNext());
        }
        cursor.close();

        avg_score = Double.parseDouble(String.format("%.2f",(avg_score / n)));

        ContentValues values = new ContentValues();
        values.put("score", avg_score);

        db.update("events", values, "event_name" + " = ?",
                new String[]{event_key});
        db.close();
    }

    public void postSurveyForm(String event_key, String email_key, int[] seekbar, String[] texts) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("event_name", event_key);
        contentValues.put("user_email", email_key);
        contentValues.put("q1", seekbar[0]);
        contentValues.put("q2", seekbar[1]);
        contentValues.put("q3", seekbar[2]);
        contentValues.put("q4", seekbar[3]);
        contentValues.put("q5", seekbar[4]);
        contentValues.put("q6", texts[0]);
        contentValues.put("q7", texts[1]);

        db.insert("survey_scores", null, contentValues);
        db.close();
    }

    public String[] getSurveyContents(String event_key, String email_key) {
        String[] answers = {"", "", "", "", "", "", ""};

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =  db.rawQuery( "select * from survey_scores where event_name= ? and user_email = ?", new String[]{event_key, email_key});

        int cursorCount = cursor.getCount();
        if (cursor.moveToFirst()) {
            do {
                answers[0] = String.valueOf(cursor.getInt(cursor.getColumnIndex("q1")));
                answers[1] = String.valueOf(cursor.getInt(cursor.getColumnIndex("q2")));
                answers[2] = String.valueOf(cursor.getInt(cursor.getColumnIndex("q3")));
                answers[3] = String.valueOf(cursor.getInt(cursor.getColumnIndex("q4")));
                answers[4] = String.valueOf(cursor.getInt(cursor.getColumnIndex("q5")));
                answers[5] = cursor.getString(cursor.getColumnIndex("q6"));
                answers[6] = cursor.getString(cursor.getColumnIndex("q7"));
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        return answers;
    }

    public boolean checkifAnsweredSurvey(String event_key, String email_key) {
        boolean answered = false;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =  db.rawQuery( "select * from survey_scores where event_name= ? and user_email = ?", new String[]{event_key, email_key});

        int cursorCount = cursor.getCount();
        if (cursor.moveToFirst()) {
            do {
                answered = true;
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        return answered;
    }

    public void sendSurvey(String event_name) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("survey_received", 1);

        db.update("event_registrations", values, "event_name = ?",
                new String[]{event_name});
        db.close();
    }

    public void eventRegistration(String event_name, String email) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("event_name", event_name);
        contentValues.put("user_email", email);
        contentValues.put("type", "R");
        contentValues.put("survey_received", "0");

        db.insert("event_registrations", null, contentValues);
        db.close();
    }

    public void updateRegistration(String event_name, String email, String type) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("event_name", event_name);
        values.put("user_email", email);
        values.put("type", type);

        db.update("event_registrations", values, "event_name = ? and user_email = ?",
                new String[]{event_name, email});
        db.close();
    }

    public RegisteredUsers readRegistered(String event_name, String email) {
        RegisteredUsers list = null;
        User user = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =  db.rawQuery( "select u.*, er.event_name, er.type from event_registrations as er join users as u " +
                "on er.user_email = u.email_address where event_name= ? and user_email = ?", new String[]{event_name, email});

        int cursorCount = cursor.getCount();
        if (cursor.moveToFirst()) {
            do {
                String first_name = cursor.getString(cursor.getColumnIndex("first_name"));
                String last_name = cursor.getString(cursor.getColumnIndex("last_name"));
                String email_address = cursor.getString(cursor.getColumnIndex("email_address"));
                String mobile_num = cursor.getString(cursor.getColumnIndex("mobile_num"));
                String password = cursor.getString(cursor.getColumnIndex("password"));
                String user_type = cursor.getString(cursor.getColumnIndex("user_type"));
                String birthday = cursor.getString(cursor.getColumnIndex("birthday"));
                String organization = cursor.getString(cursor.getColumnIndex("organization"));

                user = new User(first_name, last_name, email_address, mobile_num, password, user_type, birthday, organization);

                String type = cursor.getString(cursor.getColumnIndex("type"));

                list = new RegisteredUsers(user, type);
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        return list;
    }

    public ArrayList<RegisteredUsers> getAllRegistered(String event_name) {
        ArrayList<RegisteredUsers> list = new ArrayList<RegisteredUsers>();;
        RegisteredUsers reg_details;
        User user = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =  db.rawQuery( "select u.*, er.event_name, er.type from event_registrations as er join users as u " +
                "on er.user_email = u.email_address where event_name= ?", new String[]{event_name});

        int cursorCount = cursor.getCount();
        if (cursor.moveToFirst()) {
            do {
                String first_name = cursor.getString(cursor.getColumnIndex("first_name"));
                String last_name = cursor.getString(cursor.getColumnIndex("last_name"));
                String email_address = cursor.getString(cursor.getColumnIndex("email_address"));
                String mobile_num = cursor.getString(cursor.getColumnIndex("mobile_num"));
                String password = cursor.getString(cursor.getColumnIndex("password"));
                String user_type = cursor.getString(cursor.getColumnIndex("user_type"));
                String birthday = cursor.getString(cursor.getColumnIndex("birthday"));
                String organization = cursor.getString(cursor.getColumnIndex("organization"));

                user = new User(first_name, last_name, email_address, mobile_num, password, user_type, birthday, organization);

                String type = cursor.getString(cursor.getColumnIndex("type"));

                reg_details = new RegisteredUsers(user, type);
                list.add(reg_details);
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        return list;
    }

    public ArrayList<RegisteredUsers> getListPerType(String event_name, String u_type) {
        ArrayList<RegisteredUsers> list = new ArrayList<RegisteredUsers>();
        RegisteredUsers reg_details;
        User user = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =  db.rawQuery( "select u.*, er.event_name, er.type from event_registrations as er join users as u " +
                "on er.user_email = u.email_address where er.event_name = ? and er.type = ?", new String[]{event_name, u_type});

        int cursorCount = cursor.getCount();

        if (cursor.moveToFirst()) {
            do {
                String first_name = cursor.getString(cursor.getColumnIndex("first_name"));
                String last_name = cursor.getString(cursor.getColumnIndex("last_name"));
                String email_address = cursor.getString(cursor.getColumnIndex("email_address"));
                String mobile_num = cursor.getString(cursor.getColumnIndex("mobile_num"));
                String password = cursor.getString(cursor.getColumnIndex("password"));
                String user_type = cursor.getString(cursor.getColumnIndex("user_type"));
                String birthday = cursor.getString(cursor.getColumnIndex("birthday"));
                String organization = cursor.getString(cursor.getColumnIndex("organization"));

                user = new User(first_name, last_name, email_address, mobile_num, password, user_type, birthday, organization);

                String type = cursor.getString(cursor.getColumnIndex("type"));

                reg_details = new RegisteredUsers(user, type);
                list.add(reg_details);
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        return list;
    }

    public boolean createEvent(Event event) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("event_name", event.getEventName());
        contentValues.put("status", event.getStatus());
        contentValues.put("date", event.getDate());
        contentValues.put("time", event.getTime());
        contentValues.put("venue", event.getVenue());
        contentValues.put("survey", event.getSurvey());
        contentValues.put("qr", event.getQR());
        contentValues.put("created_by", event.getCreator());

        db.insert("events", "score", contentValues);

        return true;
    }

    public Event readEvent(String name) {
        Event event = null;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =  db.rawQuery( "select * from events where event_name= ?", new String[]{name});

        int cursorCount = cursor.getCount();
        if (cursor.moveToFirst()) {
            do {
                String event_name = cursor.getString(cursor.getColumnIndex("event_name"));
                String status = cursor.getString(cursor.getColumnIndex("status"));
                String date = cursor.getString(cursor.getColumnIndex("date"));
                String time = cursor.getString(cursor.getColumnIndex("time"));
                String venue = cursor.getString(cursor.getColumnIndex("venue"));
                Double score = cursor.getDouble(cursor.getColumnIndex("score"));
                int survey = cursor.getInt(cursor.getColumnIndex("survey"));
                byte[] qr = cursor.getBlob(cursor.getColumnIndex("qr"));
                String created_by = cursor.getString(cursor.getColumnIndex("created_by"));

                event = new Event(event_name, status, date, time, venue, score, survey, qr, created_by);
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        return event;
    }

    public void updateEvent(Event event) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("status", event.getStatus());
        values.put("date", event.getDate());
        values.put("time", event.getTime());
        values.put("venue", event.getVenue());

        db.update("events", values, "event_name" + " = ?",
                new String[]{String.valueOf(event.getEventName())});
        db.close();
    }

    public ArrayList<Event> getAllAttendeeEvents(ArrayList<String> filter_status, String email) {
        Event event;
        String where_stmt = "";
        String[] selection_columns = {};
        int n = 0;
        if (filter_status == null) {
            filter_status = new ArrayList<String>();
        }
        if (filter_status.size() == 0) {
            where_stmt = "where e2.user_email = '"+email+"' ";
        }
        else if (filter_status.size() != 0) {
            where_stmt = "where e2.user_email = '"+email+"' and (";
            for (int i = 0; i < filter_status.size(); i++) {
                where_stmt += "status = ? or ";
                selection_columns = addToArray(n, selection_columns, filter_status.get(i));
                n++;
            }
            where_stmt = where_stmt.replaceAll("or $", ")");
        }


        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =  db.rawQuery( "select * from events as e1 join event_registrations as e2 on e1.event_name  = e2.event_name " +
                ""+where_stmt+"order by status DESC", selection_columns);
        ArrayList<Event> eventList = new ArrayList<Event>();

        if (cursor.moveToFirst()) {
            do {
                String event_name = cursor.getString(cursor.getColumnIndex("event_name"));
                String status = cursor.getString(cursor.getColumnIndex("status"));
                String date = cursor.getString(cursor.getColumnIndex("date"));
                String time = cursor.getString(cursor.getColumnIndex("time"));
                String venue = cursor.getString(cursor.getColumnIndex("venue"));
                Double score = cursor.getDouble(cursor.getColumnIndex("score"));
                int survey = cursor.getInt(cursor.getColumnIndex("survey"));
                byte[] qr = cursor.getBlob(cursor.getColumnIndex("qr"));
                String created_by = cursor.getString(cursor.getColumnIndex("created_by"));

                event = new Event(event_name, status, date, time, venue, score, survey, qr, created_by);
                eventList.add(event);
            }
            while(cursor.moveToNext());
        }
        cursor.close();
        db.close();

        return eventList;
    }

    public ArrayList<Event> getAllEvents(ArrayList<String> filter_status, String email) {
        Event event;
        String where_stmt = "";
        String[] selection_columns = {};
        int n = 0;
        if (filter_status == null) {
            filter_status = new ArrayList<String>();
        }
        if (filter_status.size() == 0) {
            where_stmt = "where created_by = '"+email+"' ";
        }
        else if (filter_status.size() != 0) {
            where_stmt = "where created_by = '"+email+"' and (";
            for (int i = 0; i < filter_status.size(); i++) {
                where_stmt += "status = ? or ";
                selection_columns = addToArray(n, selection_columns, filter_status.get(i));
                n++;
            }
            where_stmt = where_stmt.replaceAll("or $", ")");
        }


        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =  db.rawQuery( "select * from events "+where_stmt+"order by status DESC", selection_columns);

        ArrayList<Event> eventList = new ArrayList<Event>();

        if (cursor.moveToFirst()) {
            do {
                String event_name = cursor.getString(cursor.getColumnIndex("event_name"));
                String status = cursor.getString(cursor.getColumnIndex("status"));
                String date = cursor.getString(cursor.getColumnIndex("date"));
                String time = cursor.getString(cursor.getColumnIndex("time"));
                String venue = cursor.getString(cursor.getColumnIndex("venue"));
                Double score = cursor.getDouble(cursor.getColumnIndex("score"));
                int survey = cursor.getInt(cursor.getColumnIndex("survey"));
                byte[] qr = cursor.getBlob(cursor.getColumnIndex("qr"));
                String created_by = cursor.getString(cursor.getColumnIndex("created_by"));

                event = new Event(event_name, status, date, time, venue, score, survey, qr, created_by);
                eventList.add(event);
            }
            while(cursor.moveToNext());
        }
        cursor.close();
        db.close();

        return eventList;
    }

    public boolean createUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("first_name", user.getFirstName());
        contentValues.put("last_name", user.getLastName());
        contentValues.put("email_address", user.getEmailAddress());
        contentValues.put("mobile_num", user.getMobileNumber());
        contentValues.put("password", user.getPassword());
        contentValues.put("user_type", user.getUserType());
        contentValues.put("birthday", user.getBirthday());
        contentValues.put("organization", user.getOrganization());

        db.insert("users", null, contentValues);
        db.close();
        return true;
    }

    public User readUser(String email) {
        User user = null;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =  db.rawQuery( "select * from users where email_address= ?", new String[]{email});

        int cursorCount = cursor.getCount();
        if (cursor.moveToFirst()) {
            do {
                String first_name = cursor.getString(cursor.getColumnIndex("first_name"));
                String last_name = cursor.getString(cursor.getColumnIndex("last_name"));
                String email_address = cursor.getString(cursor.getColumnIndex("email_address"));
                String mobile_num = cursor.getString(cursor.getColumnIndex("mobile_num"));
                String password = cursor.getString(cursor.getColumnIndex("password"));
                String user_type = cursor.getString(cursor.getColumnIndex("user_type"));
                String birthday = cursor.getString(cursor.getColumnIndex("birthday"));
                String organization = cursor.getString(cursor.getColumnIndex("organization"));

                user = new User(first_name, last_name, email_address, mobile_num, password, user_type, birthday, organization);
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        return user;
    }

    public String[] addToArray(int n, String arr[], String field) {
        String new_arr[] = new String[n+1];

        for (int i = 0; i < n; i++) {
            new_arr[i] = arr[i];
        }
        new_arr[n] = field;

        return new_arr;
    }

    public int[] addToArray(int n, int arr[], int field) {
        int new_arr[] = new int[n+1];

        for (int i = 0; i < n; i++) {
            new_arr[i] = arr[i];
        }
        new_arr[n] = field;

        return new_arr;
    }
}
