package ph.edu.dlsu.s12.eventio;

public class RegisteredUsers {
    private User user;
    private String type;
    private int survey_recieved;

    public RegisteredUsers(User user, String type) {
        this.user = user;
        this.type = type;
        this.survey_recieved = 0;
    }

    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }

    public String getRegistrationType() {
        return type;
    }
    public void setRegistrationType(String type) {
        this.type = type;
    }

    public int getSurveyReceived() {
        return survey_recieved;
    }
    public void setSurveyReceived(int survey_recieved) {
        this.survey_recieved = survey_recieved;
    }
}
