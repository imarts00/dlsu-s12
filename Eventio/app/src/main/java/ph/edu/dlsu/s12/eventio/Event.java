package ph.edu.dlsu.s12.eventio;

import java.util.ArrayList;

public class Event {
    private String event_name, status, venue;
    private String date;
    private String time;
    private int survey;
    private Double score;
    private byte[] qr;
    private ArrayList<RegisteredUsers> registered_users;
    private String created_by;

    public Event (String event_name, String status, String date, String time, String venue, Double score, int survey, byte[] qr, String created_by) {
        this.event_name = event_name;
        this.status = status;
        this.date = date;
        this.time = time;
        this.venue = venue;
        this.score = score;
        this.survey = survey;
        this.qr = qr;
        this.registered_users = null;
        this.created_by = created_by;
    }

    public String getEventName() { return event_name; }
    public void setEventName(String event_name) {
        this.event_name = event_name;
    }

    public String getStatus() { return status; }
    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() { return date; }
    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() { return time; }
    public void setTime(String time) {
        this.time = time;
    }

    public String getVenue() { return venue; }
    public void setVenue(String venue) {
        this.venue = venue;
    }

    public Double getScore() { return score; }
    public void setScore(Double score) {
        this.score = score;
    }

    public int getSurvey() { return survey; }
    public void setSurvey(int survey) {
        this.survey = survey;
    }

    public byte[] getQR() { return qr; }
    public void setQR(byte[] qr) {
        this.qr = qr;
    }

    public ArrayList<RegisteredUsers> getRegisteredUsers() { return registered_users; }
    public void setRegisteredUsers(ArrayList<RegisteredUsers> registered_users) {
        this.registered_users = registered_users;
    }

    public String getCreator() { return created_by; }
    public void setCreator(String created_by) {
        this.created_by = created_by;
    }
}
