package ph.edu.dlsu.s12.eventio;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class EventStatusAdapter extends ArrayAdapter<EventStatus> {
    private Activity activity;
    private ArrayList<EventStatus> statusArrayList;
    private boolean isFromView = false;

    public EventStatusAdapter(Activity activity, ArrayList<EventStatus> statusArrayList) {
        super(activity, R.layout.layout_status_spinner, statusArrayList);
        this.activity = activity;
        this.statusArrayList = statusArrayList;
    };

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(final int position, View convertView,
                              ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {
            LayoutInflater layoutInflator = activity.getLayoutInflater();
            convertView = layoutInflator.inflate(R.layout.layout_status_spinner, null);

            holder = new ViewHolder();
            holder.status_holder = (TextView) convertView.findViewById(R.id.status_holder);
            holder.checkbox = (CheckBox) convertView.findViewById(R.id.checkbox);

            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.status_holder.setText(statusArrayList.get(position).getStatusName());

        // To check weather checked event fire from getview() or user input
        isFromView = true;
        holder.checkbox.setChecked(statusArrayList.get(position).getState());
        isFromView = false;

        if ((position == 0)) {
            holder.checkbox.setVisibility(View.INVISIBLE);
        } else {
            holder.checkbox.setVisibility(View.VISIBLE);
        }
        holder.checkbox.setTag(position);
        holder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int getPosition = (Integer) buttonView.getTag();

                if (!isFromView) {
                    statusArrayList.get(position).toggleState(isChecked);
                }
            }
        });
        return convertView;
    }

//    public View getView(int pos, View convertView, ViewGroup parent) {
//        View rowView = convertView;
//
//        if (rowView == null) {
//            LayoutInflater inflater = activity.getLayoutInflater();
//            rowView = inflater.inflate(R.layout.layout_status_spinner, null);
//
//            ViewHolder spinnerViewHolder = new ViewHolder();
//
//            spinnerViewHolder.checkbox = (CheckBox) rowView.findViewById((R.id.checkbox));
//            spinnerViewHolder.status_holder = (TextView) rowView.findViewById((R.id.status_holder));
//
//            rowView.setTag(spinnerViewHolder);
//        }
//
//        final ViewHolder holder = (ViewHolder) rowView.getTag();
//        EventStatus status = getItem(pos);
//        holder.checkbox.setChecked(status.getState());
//        holder.status_holder.setText(status.getStatusName());
//
//        return rowView;
//    };

    static class ViewHolder{
        public CheckBox checkbox;
        public TextView status_holder;
    }
}
