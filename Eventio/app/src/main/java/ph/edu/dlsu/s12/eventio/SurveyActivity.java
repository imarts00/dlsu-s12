package ph.edu.dlsu.s12.eventio;

import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class SurveyActivity extends AppCompatActivity {

    private TextView header;
    private SeekBar q1, q2, q3, q4, q5;
    private EditText q6, q7;
    private Button btn_submit;
    private int[] seekbar_val = {0, 0, 0, 0, 0};
    private String[] texts = {"",""};
    public String email;
    public String event_name;
    public String user_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey);

        // Initialize other elements of the activity aside from the seekbars
        this.header = findViewById(R.id.header);
        this.btn_submit = findViewById(R.id.btn_submit);
        this.q6 = findViewById(R.id.q6);
        this.q7 = findViewById(R.id.q7);

        if(getIntent().getExtras() != null) {
            event_name = getIntent().getStringExtra("event_name");
            user_type = getIntent().getStringExtra("user_type");
            email = getIntent().getStringExtra("email");
        }

        dbHelper dbHelper = new dbHelper(getApplicationContext());
        Event event_details = dbHelper.readEvent(event_name);

        header.setText("Survey Form: "+event_details.getEventName());

        initSeekBars();

        // Initializes the contents of the input fields and disables actions if survey has been answered by the user
        if(dbHelper.checkifAnsweredSurvey(event_name, email)) {
            initContents(dbHelper.getSurveyContents(event_name, email));
            disableChange();
        }

        // Validates if all the input fields are not empty on button click
        // also calls the function for creating survey record and updates the registration details and event score
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateFields(q6.getText().toString(), q7.getText().toString())) {
                    Event event_details = dbHelper.readEvent(event_name);
                    texts[0] = q6.getText().toString();
                    texts[1] = q7.getText().toString();

                    dbHelper.postSurveyForm(event_name, email, seekbar_val, texts);
                    dbHelper.updateRegistration(event_name, email, "A");
                    dbHelper.updateEventScore(event_name);

                    Intent intent = new Intent(SurveyActivity.this, ViewEventActivity.class);
                    intent.putExtra("event_name", event_name);
                    intent.putExtra("user_type", user_type);
                    intent.putExtra("email", email);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    // Function to validate if all the user input fields are not empty
    private boolean validateFields(String q6, String q7) {
        boolean res = true;

        if (q6.trim().length() == 0) {
            res = false;
        }
        if (q7.trim().length() == 0) {
            res = false;
        }

        if(!res) {
            Toast.makeText(getApplicationContext(), "Missing field!", Toast.LENGTH_SHORT).show();
        }
        return res;
    }

    // Function to initialize all the seekbar elements and set the initial values
    private void initContents(String[] answers) {
        this.q1 = findViewById(R.id.q1);
        this.q2 = findViewById(R.id.q2);
        this.q3 = findViewById(R.id.q3);
        this.q4 = findViewById(R.id.q4);
        this.q5 = findViewById(R.id.q5);

        q1.setProgress(Integer.parseInt(answers[0]));
        q2.setProgress(Integer.parseInt(answers[1]));
        q3.setProgress(Integer.parseInt(answers[2]));
        q4.setProgress(Integer.parseInt(answers[3]));
        q5.setProgress(Integer.parseInt(answers[4]));
        q6.setText(answers[5]);
        q7.setText(answers[6]);
    }

    // Initializes the seekbar listener and values by assigning a customized seekbar listener
    private void initSeekBars() {
        this.q1 = findViewById(R.id.q1);
        this.q2 = findViewById(R.id.q2);
        this.q3 = findViewById(R.id.q3);
        this.q4 = findViewById(R.id.q4);
        this.q5 = findViewById(R.id.q5);

        SeekBar.OnSeekBarChangeListener myListener = new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                switch (seekBar.getId()) {
                    case R.id.q1:
                        seekbar_val[0] = i;
                        break;
                    case R.id.q2:
                        seekbar_val[1] = i;
                        break;
                    case R.id.q3:
                        seekbar_val[2] = i;
                        break;
                    case R.id.q4:
                        seekbar_val[3] = i;
                        break;
                    case R.id.q5:
                        seekbar_val[4] = i;
                        break;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        };

        q1.setOnSeekBarChangeListener(myListener);
        q2.setOnSeekBarChangeListener(myListener);
        q3.setOnSeekBarChangeListener(myListener);
        q4.setOnSeekBarChangeListener(myListener);
        q5.setOnSeekBarChangeListener(myListener);
    }

    // Disables onclick events and disables the input fields of the activity
    public void disableChange() {
        this.q1 = findViewById(R.id.q1);
        this.q2 = findViewById(R.id.q2);
        this.q3 = findViewById(R.id.q3);
        this.q4 = findViewById(R.id.q4);
        this.q5 = findViewById(R.id.q5);
        this.btn_submit = findViewById(R.id.btn_submit);
        this.q6 = findViewById(R.id.q6);
        this.q7 = findViewById(R.id.q7);

        q1.setOnTouchListener((new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                return true;
            }
        }));
        q2.setOnTouchListener((new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                return true;
            }
        }));
        q3.setOnTouchListener((new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                return true;
            }
        }));
        q4.setOnTouchListener((new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                return true;
            }
        }));
        q5.setOnTouchListener((new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                return true;
            }
        }));

        q6.setEnabled(false);
        q7.setEnabled(false);

        btn_submit.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(SurveyActivity.this, ViewEventActivity.class);
        intent.putExtra("event_name", event_name);
        intent.putExtra("user_type", user_type);
        intent.putExtra("email", email);
        startActivity(intent);
        finish();
    }
}
